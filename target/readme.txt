How to Execute
0. Copy the file alphabet-soup-1.0-SNAPSHOT.jar to a folder.
1. Copy the input file, the one with the grid to the folder above.
2. Invoke a Windows console command prompt and change to that directory.
3. execute: java -jar alphabet-soup-1.0-SNAPSHOT.jar
  A prompt will appear: Enter file name or 'quit' to end:
                        <path/filename> to the file that contains the grid.
		Example: java -jar alphabet-soup-1.0-SNAPSHOT.jar 
		         Enter file name or quit to end:soup.txt
					  hello 00:44
					  good  40:43
					  bye   13:11
				Enter file name or quit to end:	end
                Done!				