package words.soup;

import java.util.ArrayList;

/**
 * Created by Rey on 4/20/2019.
 */
public class LineCoordinates {

    private String line;
    private ArrayList<String> coordinates;

    public LineCoordinates(String line, ArrayList<String> coordinates)
    {
        this.line = line;
        this.coordinates = new ArrayList<String>(coordinates);
    }

    public String getLine()
    {
        return this.line;
    }

    public ArrayList<String> getCoordinates()
    {
        return this.coordinates;
    }

    private void setLine(String line)
    {
        this.line = line;
    }

    private void setCoordinates(ArrayList<String> coordinates)
    {
        this.coordinates = coordinates;
    }
}
