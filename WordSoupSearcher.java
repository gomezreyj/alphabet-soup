package words.soup;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by Rey on 4/5/2019.
 */

/*
The program is to accept a file as input. The file is an ASCII text file containing the word search board along with the words that need to be found.
The file contains three parts. The first part is the first line, and specifies the number of rows and columns in the grid of characters, separated by a space.
The second part provides the grid of characters in the word search. The third part in the file specifies the words to be found.
The first line indicates how many following lines in the file contain the rows of characters that make up the word search grid.
Each row in the word search grid will have the specified number of columns of characters, each separated with a space.
The remaining lines in the file specify the words to be found.

 */

/*
This enum set the way to create the diagonal
*/
enum Direction
{
    LEFT_TO_RIGHT, RIGHT_TO_LEFT, TOPLEFT_TO_RIGHTBOTTOM, TOPRIGHT_TO_LEFTBOTTOM
}
/*
This enum set the type of line where to find the word
*/
enum LineType
{
    VERTICALS, HORIZONTALS, DIAGONAL
}

/*
MAIN CLASS
 */
public class WordSoupSearcher {

    private ArrayList<LineCoordinates> horizontalLines;
    private ArrayList<LineCoordinates> verticalLines;
    private ArrayList<StringBuffer> verticalBufferLines;
    private ArrayList<LineCoordinates> diagonals;
    private List<String> words;
    private List<String> soup;

    public WordSoupSearcher()
    {
        this.diagonals = new ArrayList<LineCoordinates>();
        this.horizontalLines = new ArrayList<LineCoordinates>();
        this.verticalLines = new ArrayList<LineCoordinates>();
        this.verticalBufferLines = new ArrayList<StringBuffer>();
        this.words = new ArrayList<String>();
        this.soup = new ArrayList<String>();
    }

    public ArrayList<LineCoordinates> getVerticalLines()
    {
        return this.verticalLines;
    }

    public ArrayList<LineCoordinates> getHorizontalLines()
    {
        return this.horizontalLines;
    }

    public ArrayList<LineCoordinates> getDiagonals()
    {
        return this.diagonals;
    }

    public List<String> getSoup()
    {
        return this.soup;
    }

    public List<String> getWords()
    {
        return this.words;
    }

    /*
     Description: This method load the horizontals and verticals line into a list.
     Input: lines = a list of lines from the grid ( horizontals and verticals ). DataType = List<String>
     */
    private void setHorizontalsAndVerticals(List<String> lines)
    {

        try {
            String dimensions = lines.get(0);

            //getting the number of rows from the first line of the file
            int rows = Character.getNumericValue(dimensions.charAt(0));

            //getting the number of cols from the first line of the file
            int cols = Character.getNumericValue(dimensions.charAt(2));

            for ( int k = 0; k < cols; k++)
            {
                verticalBufferLines.add(new StringBuffer());
            }

            ArrayList<String> coord = new ArrayList<String>();

            // getting a line a add the horizontal line and a character for each vertical line
            for ( int i = 1, x = 0; i <= rows; i++, x++ )
            {
                for ( int y = 0; y < cols; y++) {
                    coord.add(String.valueOf(x) + String.valueOf(y));
                }
                // just read the line from the file and make it as a horizontal line
                LineCoordinates lc = new LineCoordinates(lines.get(i).replaceAll("\\s+", ""),coord);
                horizontalLines.add(lc);
                coord.clear();
                // let build the vertical lines
                for ( int j = 0; j < cols; j++)
                {
                    verticalBufferLines.get(j).append(horizontalLines.get(i - 1).getLine().charAt(j));
                }
            }
            coord.clear();
            for ( int col = 0; col < cols; col++) {
                for (int row = 0; row < rows; row++)
                    coord.add(String.valueOf(row) + String.valueOf(col));
             LineCoordinates lc = new LineCoordinates(verticalBufferLines.get(col).toString(),coord);
             verticalLines.add(lc);
             coord.clear();
            }
        }
        catch (Exception error)
        {
            error.printStackTrace();
        }
    }

    /*
      Description: This method built the triangular inferior diagonals from the grid.
       Input: soup = the grid.  DataType: List<String>
              direction = in which traverse the grid. DataType = Enum
     */

    public void getTriangularInferior(List<String> soup, Direction direction)
    {
        int rows = Character.getNumericValue(soup.get(0).charAt(0));
        int cols = Character.getNumericValue(soup.get(0).charAt(2));

        ArrayList<String> coord = new ArrayList<String>();

        if ( direction == Direction.LEFT_TO_RIGHT) {
            if (cols > rows) {
                for (int i = rows - 1; i > 0; i--) {
                    StringBuffer diagonalWord = new StringBuffer();
                    for (int j = i, k = 0; j <= rows - 1; k++, j++) {
                        //System.out.println(j + "," + k);
                        coord.add(String.valueOf(j) + String.valueOf(k));
                        diagonalWord.append(soup.get(j + 1).replaceAll("\\s+", "").charAt(k));
                    }
                    LineCoordinates lc = new LineCoordinates(diagonalWord.toString(),coord);
                    diagonals.add(lc);
                    coord.clear();
                }
            } else
                for (int j = 0; j < cols - 1; j++) {
                    StringBuffer diagonalWord = new StringBuffer();
                    for (int i = rows - 1, k = j; k >= 0; i--, k--) {
                        //System.out.println(i + "," + k);
                        coord.add(String.valueOf(i) + String.valueOf(k));
                        diagonalWord.append(soup.get(i + 1).replaceAll("\\s+", "").charAt(k));
                    }
                    LineCoordinates lc = new LineCoordinates(diagonalWord.toString(),coord);
                    diagonals.add(lc);
                    coord.clear();
                }
        }
        else
        if ( direction == Direction.RIGHT_TO_LEFT)
        {
           if ( cols > rows )
           {
              for ( int i = cols - 1,  l = 0; l < rows -1; i--, l++) {
                  StringBuffer diagonalWord = new StringBuffer();
                  for (int j = i, k = rows - 1; j <= cols - 1; k--, j++) {
                      //System.out.println(k + "," + j);
                      coord.add(String.valueOf(k) + String.valueOf(j));
                      diagonalWord.append(soup.get(k + 1).replaceAll("\\s+", "").charAt(j));
                  }
                  LineCoordinates lc = new LineCoordinates(diagonalWord.toString(),coord);
                  diagonals.add(lc);
                  coord.clear();
              }
           }
           else
           {
               for ( int i = cols - 1; i > 0; i--) {
                   StringBuffer diagonalWord = new StringBuffer();
                   for (int j = i, k = rows - 1; j <= cols - 1; k--, j++) {
                       //System.out.println(k + "," + j);
                       coord.add(String.valueOf(k) + String.valueOf(j));
                       diagonalWord.append(soup.get(k + 1).replaceAll("\\s+", "").charAt(j));
                   }
                   LineCoordinates lc = new LineCoordinates(diagonalWord.toString(),coord);
                   diagonals.add(lc);
                   coord.clear();
               }
           }
        }

    }

      /*
      Description: This method built the triangular superior diagonals from the grid.
       Input: soup = the grid.  DataType: List<String>
              direction = in which traverse the grid. DataType = Enum
     */

    public void getTriangularSuperior(List<String> soup, Direction direction)
    {
        int rows = Character.getNumericValue(soup.get(0).charAt(0));
        int cols = Character.getNumericValue(soup.get(0).charAt(2));
        ArrayList<String> coord = new ArrayList<String>();

        if ( direction == Direction.LEFT_TO_RIGHT) {
            if (cols > rows) {
                for (int i = cols - 1, l = 0; l < rows - 1; i--, l++) {
                    StringBuffer diagonalWord = new StringBuffer();
                    for (int j = i, k = 0; j <= cols - 1; k++, j++) {
                        //System.out.println(k + "," + j);
                        coord.add(String.valueOf(k) + String.valueOf(j));
                        diagonalWord.append(soup.get(k + 1).replaceAll("\\s+", "").charAt(j));
                    }
                    LineCoordinates lc = new LineCoordinates(diagonalWord.toString(),coord);
                    diagonals.add(lc);
                    coord.clear();
                }
            } else // cols < rows
                for (int i = cols - 1; i > 0; i--) {
                    StringBuffer diagonalWord = new StringBuffer();
                    for (int j = i, k = 0; j <= cols - 1; k++, j++) {
                        //System.out.println(k + "," + j);
                        coord.add(String.valueOf(k) + String.valueOf(j));
                        diagonalWord.append(soup.get(k + 1).replaceAll("\\s+", "").charAt(j));
                    }
                    LineCoordinates lc = new LineCoordinates(diagonalWord.toString(),coord);
                    diagonals.add(lc);
                    coord.clear();
                }
        }
        else
        if ( direction == Direction.RIGHT_TO_LEFT ) {
         if ( cols > rows )
         {
            for ( int i = 0; i < rows - 1; i++) {
                StringBuffer diagonalWord = new StringBuffer();
                for (int j = i, k = 0; j >= 0; k++, j--) {
                    //System.out.println(j + "," + k);
                    coord.add(String.valueOf(j) + String.valueOf(k));
                    diagonalWord.append(soup.get(j + 1).replaceAll("\\s+", "").charAt(k));
                }
                LineCoordinates lc = new LineCoordinates(diagonalWord.toString(),coord);
                diagonals.add(lc);
                coord.clear();
            }
         }
         else
         {
             for ( int i = 0; i < cols - 1; i++) {

                 StringBuffer diagonalWord = new StringBuffer();
                 for (int j = i, k = 0; j >= 0; k++, j--) {
                     //System.out.println(j + "," + k);
                     coord.add(String.valueOf(j) + String.valueOf(k));
                     diagonalWord.append(soup.get(j + 1).replaceAll("\\s+", "").charAt(k));
                 }
                 LineCoordinates lc = new LineCoordinates(diagonalWord.toString(),coord);
                 diagonals.add(lc);
                 coord.clear();
             }
         }
        }
    }

      /*
      Description: This method built the main diagonals from the grid.
       Input: soup = the grid.  DataType: List<String>
              direction = in which the diagonals will be traverse. DataType = Enum
     */

    public void getDiagonal(List<String> soup, Direction direction)
    {
        int rows = Character.getNumericValue(soup.get(0).charAt(0));
        int cols = Character.getNumericValue(soup.get(0).charAt(2));
        ArrayList<String> coord = new ArrayList<String>();

        if ( direction == Direction.TOPLEFT_TO_RIGHTBOTTOM ) {
            if (cols > rows)
                for (int i = 0; i < cols - rows + 1; i++) {
                    StringBuffer diagonalWord = new StringBuffer();
                    for (int j = cols - 1 - i, k = rows - 1; k >= 0; k--, j--) {
                        //System.out.println(k + "," + j);
                        coord.add(String.valueOf(k) + String.valueOf(j));
                        //System.out.print((soup.get(k + 1).replaceAll("\\s+", "").charAt(j)));
                        diagonalWord.append(soup.get(k + 1).replaceAll("\\s+", "").charAt(j));
                    }
                    LineCoordinates lc = new LineCoordinates(diagonalWord.toString(),coord);
                    diagonals.add(lc);
                    coord.clear();
                }
            else if (cols == rows) {
                StringBuffer diagonalWord = new StringBuffer();
                for (int i = 0; i < cols; i++) {
                    //System.out.println(i + "," + i);
                    coord.add(String.valueOf(i) + String.valueOf(i));
                    diagonalWord.append(soup.get(i + 1).replaceAll("\\s+", "").charAt(i));
                }
                LineCoordinates lc = new LineCoordinates(diagonalWord.toString(),coord);
                diagonals.add(lc);
                coord.clear();
            } else
                for (int i = 0; i < rows - cols + 1; i++) {
                    StringBuffer diagonalWord = new StringBuffer();
                    for (int j = rows - 1 - i, k = cols - 1; k >= 0; k--, j--) {
                        //System.out.println(j + "," + k);
                        coord.add(String.valueOf(j) + String.valueOf(k));
                        diagonalWord.append(soup.get(j + 1).replaceAll("\\s+", "").charAt(k));
                    }
                    LineCoordinates lc = new LineCoordinates(diagonalWord.toString(),coord);
                    diagonals.add(lc);
                    coord.clear();
                }
        }
        else // TOPRIGHT_TO_BOTTOMLEFT
        {
            if (cols > rows)
                for (int i = 0; i < cols - rows + 1; i++) {
                    StringBuffer diagonalWord = new StringBuffer();
                    for (int j = i, k = rows - 1; k >= 0; k--, j++) {
                        //System.out.println(k + "," + j);
                        coord.add(String.valueOf(k) + String.valueOf(j));
                        //System.out.print((soup.get(k + 1).replaceAll("\\s+", "").charAt(j)));
                        diagonalWord.append(soup.get(k + 1).replaceAll("\\s+", "").charAt(j));
                    }
                    LineCoordinates lc = new LineCoordinates(diagonalWord.toString(),coord);
                    diagonals.add(lc);
                    coord.clear();
                }
            else
            if (cols == rows) {
                StringBuffer diagonalWord = new StringBuffer();
                for (int i = cols - 1, k = 0; i >= 0; i--, k++) {
                    //System.out.println(i + "," + k);
                    coord.add(String.valueOf(i) + String.valueOf(k));
                    diagonalWord.append(soup.get(i + 1).replaceAll("\\s+", "").charAt(k));
                }
                LineCoordinates lc = new LineCoordinates(diagonalWord.toString(),coord);
                diagonals.add(lc);
                coord.clear();
            }
            else
            {
                for (int i = 0; i < rows - cols + 1; i++) {
                  StringBuffer diagonalWord = new StringBuffer();
                  for ( int j = cols - 1 + i, k = 0;  k <= cols - 1; k++, j--) {
                      //System.out.println(j + "," + k);
                      coord.add(String.valueOf(j) + String.valueOf(k));
                      diagonalWord.append(soup.get(j + 1).replaceAll("\\s+", "").charAt(k));
                  }
                    LineCoordinates lc = new LineCoordinates(diagonalWord.toString(),coord);
                    diagonals.add(lc);
                    coord.clear();
                }

            }
        }
    }

       /*
      Description: This method search the line for the word, either anverse or reverse.
      If the word in found it then will return the coordinates (row and column) else return null.
       Input: line = where to find the word  DataType: String
              word = the word to be search it for. DataType: String
       Output: the coordinates (x,y) where appeared the word. Null if does not appeared.
     */

    public String getCoordinates(String line, String word)
    {
        int startIndex = line.indexOf(word);
        int endIndex = -1;

        // looking forward
        if ( startIndex != -1 )
        {
            endIndex = startIndex + word.length() - 1;
            return String.valueOf(startIndex) + String.valueOf(endIndex);
        }
        // looking in reverse
        if ( startIndex == -1)
        {
            StringBuilder reverseWord = new StringBuilder();
            // append a string into StringBuilder input1
            reverseWord.append(word);
            // reverse StringBuilder input1
            reverseWord = reverseWord.reverse();
            startIndex = line.indexOf(reverseWord.toString());
            if ( startIndex != -1 )
            {
                endIndex = startIndex + reverseWord.length() - 1;
                return String.valueOf(endIndex) + String.valueOf(startIndex);
            }

        }
        return null;
    }

    /*
    Description: This method iterate over the VERTICALS, HORIZONTALS and DIAGONALS and search for the word. For every word
    found it it will print the row and column where the word start and end within the grid.
    Input: lines = the list of lines to where to look for the word. DataType = ArrayList<LineCoordinates>
           word = the word to be search. DataType = String
           lineType = the specific type of line ( vertical, horizontal or diagonal )
    Output: the coordinates ( row and colum in the format start(x,y):end(x,y)

     */

    public String findWord(ArrayList<LineCoordinates> lines, String word, LineType lineType)
    {
        LineCoordinates line;
        String coordinates;

        if ( lineType == LineType.VERTICALS )
        {
            for ( int l = 0; l < lines.size(); l++)
            {
                line = lines.get(l);
                coordinates = getCoordinates(line.getLine().toLowerCase(),word.toLowerCase());
                if ( coordinates != null )
                    return line.getCoordinates().get(Character.getNumericValue(coordinates.charAt(0))) + ":" + line.getCoordinates().get(Character.getNumericValue(coordinates.charAt(1)));
            }
        }
        else
        if ( lineType == LineType.HORIZONTALS )
        {
            for ( int l = 0; l < lines.size(); l++)
            {
                line = lines.get(l);
                coordinates = getCoordinates(line.getLine().toLowerCase(),word.toLowerCase());
                if ( coordinates != null )
                    return line.getCoordinates().get(Character.getNumericValue(coordinates.charAt(0))) + ":" + line.getCoordinates().get(Character.getNumericValue(coordinates.charAt(1)));
            }
        }
        else
        if ( lineType == LineType.DIAGONAL )
        {
            for ( int l = 0; l < lines.size(); l++)
            {
                line = lines.get(l);
                coordinates = getCoordinates(line.getLine().toLowerCase(),word.toLowerCase());
                if ( coordinates != null )
                    return line.getCoordinates().get(Character.getNumericValue(coordinates.charAt(0))) + ":" + line.getCoordinates().get(Character.getNumericValue(coordinates.charAt(1)));
            }
        }

       return null;
    }

    /*
     Description: This method read the file and populate a list with the grid. At the same time populate a list of words.
     If the file does not exits an error is displayed.
      Input: filename = the file in the format of : C:\dir...\alphabet-soup.txt
      where alphabet-soup.txt contains the grid.
     */
    public void LoadSoupFromFile(String fileName)
    {
        try {
            soup  = Files.readAllLines(Paths.get(fileName));
            int rows = Character.getNumericValue(soup.get(0).charAt(0));
            for ( int indexWords = rows + 1; indexWords < soup.size(); indexWords++)
            {
                words.add(soup.get(indexWords));
            }
        }
        catch (Exception error)
        {
            System.out.println("File not found: " + fileName);
            Path currentRelativePath = Paths.get("");
            String s = currentRelativePath.toAbsolutePath().toString();
            System.out.println("Current path: " + s);
            System.exit(-1);
        }
    }


    static public List<String> TestWordSoup()
    {
        List<String> soupOfWords = new ArrayList<String>();

        soupOfWords.add("5x5");
        soupOfWords.add("h a s d f");
        soupOfWords.add("g e y b h");
        soupOfWords.add("j k l z x");
        soupOfWords.add("c v b l n");
        soupOfWords.add("g o o d o");



//          soupOfWords.add("3x3");
//          soupOfWords.add("a b c");
//          soupOfWords.add("g h i");
//          soupOfWords.add("m n o");

//        soupOfWords.add("6x4");
//        soupOfWords.add("a b c d");
//        soupOfWords.add("g h i e");
//        soupOfWords.add("m n o f");
//        soupOfWords.add("s t u w");
//        soupOfWords.add("a b c d");
//        soupOfWords.add("g h i j");

//        soupOfWords.add("4x6");
//        soupOfWords.add("a b c d e f");
//        soupOfWords.add("g h i e r x");
//        soupOfWords.add("m n o f w h");
//        soupOfWords.add("s t u w q h");


//        soupOfWords.add("5x7");
//        soupOfWords.add("m n o p q r t");
//        soupOfWords.add("s t u v w x e");
//        soupOfWords.add("y z a b c d h");
//        soupOfWords.add("e f g h i j j");
//        soupOfWords.add("e f g h i j x");

//          soupOfWords.add("6x6");
//          soupOfWords.add("m n o p q r");
//          soupOfWords.add("s t u v w x");
//          soupOfWords.add("y z a b c d");
//          soupOfWords.add("e f g h i j");
//          soupOfWords.add("y z a b c d");
//          soupOfWords.add("e f g h i j");

        return soupOfWords;
    }


    public static void main (String args[])
    {
        String s;
        boolean execute = true;

        WordSoupSearcher ws  = new WordSoupSearcher();

        while (execute) {
            Scanner keyboard = new Scanner(System.in);
            // Get the filename, or...
            System.out.println("Enter file name or 'quit' to end:");

            s = keyboard.nextLine();

            if (s.toUpperCase().equals("QUIT"))
            {
                System.out.println("Done!");
                execute = false;
            }
            else
            {
                ws.LoadSoupFromFile(s);
                ws.getTriangularInferior(ws.getSoup(), Direction.RIGHT_TO_LEFT);
                ws.getTriangularInferior(ws.getSoup(), Direction.LEFT_TO_RIGHT);
                ws.getTriangularSuperior(ws.getSoup(), Direction.RIGHT_TO_LEFT);
                ws.getTriangularSuperior(ws.getSoup(), Direction.LEFT_TO_RIGHT);

                ws.getDiagonal(ws.getSoup(), Direction.TOPRIGHT_TO_LEFTBOTTOM);
                ws.getDiagonal(ws.getSoup(), Direction.TOPLEFT_TO_RIGHTBOTTOM);

                ws.setHorizontalsAndVerticals(ws.getSoup());

                for ( String word : ws.getWords()) {
                    String foundWordAt = null;

                    foundWordAt = ws.findWord(ws.getVerticalLines(), word, LineType.VERTICALS);
                    if (foundWordAt != null) {
                        System.out.println(word + " " + foundWordAt);
                    }
                    else
                    {
                      foundWordAt = ws.findWord(ws.getHorizontalLines(), word, LineType.HORIZONTALS);
                      if (foundWordAt != null) {
                            System.out.println(word + " " + foundWordAt);
                      }
                    }
                    if ( foundWordAt == null )
                    {
                        foundWordAt = ws.findWord(ws.getDiagonals(), word, LineType.DIAGONAL);
                        if (foundWordAt != null) {
                            System.out.println(word + " " + foundWordAt);
                        }
                    }
                }
            }
        }
    }
}
